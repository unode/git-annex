#!/usr/bin/env bash

wget https://downloads.kitenet.net/git-annex/linux/current/git-annex-standalone-amd64.tar.gz
tar xf git-annex-standalone-amd64.tar.gz
VERSION=$(git-annex.linux/git-annex version | grep "git-annex version:" | cut -d ' ' -f 3)
mv git-annex.linux "git-annex-${VERSION}"
rm -f git-annex-latest
ln -s "git-annex-${VERSION}" git-annex-latest

rm -f git-annex-standalone-amd64.tar.gz
